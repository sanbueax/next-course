export default [
    {
        email: "admin@mail.com",
        password: "admin123",
        isAdmin: true
    },
    {
        email: "jay@mail.com",
        password: "jay123",
        isAdmin: false
    }
]