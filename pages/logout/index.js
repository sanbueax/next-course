import { useContext, useEffect } from 'react';
import UserContext from '../../UserContext';
import Router from 'next/router';
import Head from 'next/head';

export default function index(){
	const {unsetUser, setuser} = useContext(UserContext)

	useEffect(() => {
		unsetUser();
		// setUser({email: null})
		Router.push('/login');
	}, [])

	return null
}