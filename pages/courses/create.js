import { useState } from 'react';
import { Form, Button, Container } from 'react-bootstrap';

export default function create(){

	const [courseId, setCourseID] = useState("");
	const [courseName, setCourseName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [startDate, setStartDate] = useState("");
	const [endDate, setEndDate] = useState("");	

	function createdCourse(e){
		e.preventDefault();

		console.log(`${courseName} with ID: ${courseId} is set to start on ${startDate} for PhP ${price} per slot.`);

		setCourseID("");
		setCourseName("");
		setDescription("");
		setPrice(0);
		setStartDate("");
		setEndDate("");

		alert("Successfully created a course!");
	}

    return (
    	<Container>
        	<Form onSubmit={(e) => createdCourse(e)}>
	            <Form.Group controlId="courseId">
	                <Form.Label>Course ID</Form.Label>
	                <Form.Control type="text" placeholder="Course ID" value={courseId} onChange={e => setCourseID(e.target.value)} required/>
	            </Form.Group>

	            <Form.Group controlId="courseName">
	                <Form.Label>Course Name</Form.Label>
	                <Form.Control type="text" placeholder="Course Name" value={courseName} onChange={e => setCourseName(e.target.value)} required/>
	            </Form.Group>

	            <Form.Group controlId="description">
	                <Form.Label>Description</Form.Label>
	                <Form.Control type="text" placeholder="Description" value={description} onChange={e => setDescription(e.target.value)} required/>
	            </Form.Group>

	            <Form.Group controlId="price">
	                <Form.Label>Price</Form.Label>
	                <Form.Control type="number" placeholder="Price" value={price} onChange={e => setPrice(e.target.value)} required/>
	            </Form.Group>

	            <Form.Group controlId="startDate">
	                <Form.Label>Start Date</Form.Label>
	                <Form.Control type="date" placeholder="Start Date" value={startDate} onChange={e => setStartDate(e.target.value)} required/>
	            </Form.Group>

	            <Form.Group controlId="endDate">
	                <Form.Label>End Date</Form.Label>
	                <Form.Control type="date" placeholder="End Date" value={endDate} onChange={e => setEndDate(e.target.value)} required/>
	            </Form.Group> 
	            <Button variant="primary" type="submit">Create</Button> 
	        </Form>    		
    	</Container>	
    )	
}